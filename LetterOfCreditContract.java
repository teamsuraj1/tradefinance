package Financetrade;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Default;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;


import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

@Contract(name = "Financetrade", info = @Info(title = "Financetrade contract", description = "Financetrade Contract to Track Letter Credit Acceptance from Buyer Bank to Seller", version = "0.0.1-SNAPSHOT"))
@Default
public class LetterOfCreditContract
{

	private final Genson genson = new GensonBuilder().setSkipNull(true).create();
	public enum LetterOfCreditError
	{
		LETTER_OF_CREDIT_DOES_NOT_EXIST,
		LETTER_OF_CREDIT_ALREADY_EXIST;
	}
	
	@Transaction()
	public void initLedger(final Context ctx)
	{
		
	}
	
	@Transaction()
	public LetterOfCredit createLetterOfCredit(Context ctx, String id, String expiryDate, String buyer, String bank, String seller, String amount)
	{
		
		ChaincodeStub stub = ctx.getStub();
		String locAdded	= stub.getStringState(id);
		if(!(locAdded.isEmpty()))
		{
			String errorMessage = String.format("Letter Of Credit id:%s already exists", id);
			System.out.println(errorMessage);
			throw new ChaincodeException(errorMessage, LetterOfCreditError.LETTER_OF_CREDIT_ALREADY_EXIST.toString());
		}
		
		LetterOfCredit credit = new LetterOfCredit();
		credit.setAmount(amount);
		credit.setBank(bank);
		credit.setBuyer(buyer);
		credit.setExpiryDate(expiryDate);
		credit.setId(id);
		credit.setSeller(seller);
		credit.setStatus(LetterOfCreditStatus.CREATED);
		
		String locInStr	= genson.serialize(credit);
		stub.putStringState(id, locInStr);
		return credit;
	}
	
	
	@Transaction()
	public LetterOfCredit issueLetterOfCredit(Context ctx, String id)
	{
		
		ChaincodeStub stub = ctx.getStub();
		String locAdded	= stub.getStringState(id);
		if(locAdded.isEmpty())
		{
			String errorMessage = String.format("Letter Of Credit id:%s doesn't exists", id);
			System.out.println(errorMessage);
			throw new ChaincodeException(errorMessage, LetterOfCreditError.LETTER_OF_CREDIT_DOES_NOT_EXIST.toString());
		}
		
		
		LetterOfCredit loc	= genson.deserialize(locAdded,LetterOfCredit.class);
		loc.setStatus(LetterOfCreditStatus.ISSUED);
		
		String locInStr	= genson.serialize(loc);
		stub.putStringState(id, locInStr);
		return loc;
	}
	
	@Transaction()
	public LetterOfCredit acceptLetterOfCredit(Context ctx, String id)
	{
		
		ChaincodeStub stub = ctx.getStub();
		String locAdded	= stub.getStringState(id);
		if(locAdded.isEmpty())
		{
			String errorMessage = String.format("Letter Of Credit id:%s doesn't exists", id);
			System.out.println(errorMessage);
			throw new ChaincodeException(errorMessage, LetterOfCreditError.LETTER_OF_CREDIT_DOES_NOT_EXIST.toString());
		}
		
		
		LetterOfCredit loc	= genson.deserialize(locAdded,LetterOfCredit.class);
		loc.setStatus(LetterOfCreditStatus.ACCEPTED);
		
		String locInStr	= genson.serialize(loc);
		stub.putStringState(id, locInStr);
		return loc;
	}

	@Transaction()
	public LetterOfCredit viewLetterOfCredit(Context ctx, String id)
	{
		
		ChaincodeStub stub = ctx.getStub();
		String locAdded	= stub.getStringState(id);
		if(locAdded.isEmpty())
		{
			String errorMessage = String.format("Letter Of Credit id:%s doesn't exists", id);
			System.out.println(errorMessage);
			throw new ChaincodeException(errorMessage, LetterOfCreditError.LETTER_OF_CREDIT_DOES_NOT_EXIST.toString());
		}
		LetterOfCredit loc	= genson.deserialize(locAdded,LetterOfCredit.class);
		return loc;
	}

}
