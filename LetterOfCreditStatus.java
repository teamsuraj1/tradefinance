package Financetrade;

public enum LetterOfCreditStatus
{
	CREATED,
	ISSUED,
	ACCEPTED,
	REJECTED
}
