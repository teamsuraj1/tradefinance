package Financetrade;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

@DataType
public class LetterOfCredit
{
	
	@Property
	private String id;
	
	@Property
	private String expiryDate;
	
	@Property
	private String buyer;
	
	@Property
	private String bank;
	
	@Property
	private String seller;
	
	@Property
	private String amount;
	
	@Property
	private LetterOfCreditStatus status;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getExpiryDate()
	{
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate)
	{
		this.expiryDate = expiryDate;
	}

	public String getBuyer()
	{
		return buyer;
	}

	public void setBuyer(String buyer)
	{
		this.buyer = buyer;
	}

	public String getBank()
	{
		return bank;
	}

	public void setBank(String bank)
	{
		this.bank = bank;
	}

	public String getSeller()
	{
		return seller;
	}

	public void setSeller(String seller)
	{
		this.seller = seller;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public LetterOfCreditStatus getStatus()
	{
		return status;
	}

	public void setStatus(LetterOfCreditStatus status)
	{
		this.status = status;
	}
	
	public LetterOfCredit()
	{
		
	}
}
